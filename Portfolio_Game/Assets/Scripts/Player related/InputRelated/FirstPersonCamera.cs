using UnityEngine;

public class FirstPersonCamera : MonoBehaviour
{

    [SerializeField]
    private Transform playBod;

    public float sensitivity = 100f;
    private float originalSensitivity;
    private float xRotation = 0f;

    [SerializeField]
    private bool clamp = true;
    [SerializeField] private Vector2 defaultRotation;


    private void Start()
    {
        //rotating to the default position
        xRotation = defaultRotation.x;
        playBod.Rotate(Vector3.up * defaultRotation.y);        

        ServiceLocator.fpsCamInstance = this;
        originalSensitivity = sensitivity;
        try
        {
          if (playBod == null) playBod = transform.parent.transform;
        }
        catch
        {   
            //The catch will only happen when no playerBody is attached and the camera does not have any parent GameObject
            Debug.LogError("The First-Person camera (" + gameObject + ") has no parent-body to rotate around correctly.");
        }
    }


    private void Update()
    {
        float mx = Input.GetAxis("Mouse X") * sensitivity * Time.deltaTime;     //Getting player mouse Input
        float my = Input.GetAxis("Mouse Y") * sensitivity * Time.deltaTime;

        xRotation -= my;
        if (clamp) xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, 0f, transform.eulerAngles.z); //X-Rotation of the Camera
        playBod.Rotate(Vector3.up * mx);        //Y rotation of the Camera
    }

    //multiply with negative number to reset value to original!
    public void ChangeSensitivity(float value)
    {
        if(value > 0)
        {

            sensitivity *= value;
        }
        else
        {
            sensitivity = originalSensitivity;
        }
    }
}
