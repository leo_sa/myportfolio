using UnityEngine;
using UnityEngine.UI;

public class PlayerInteraction : MonoBehaviour
{
    [SerializeField] private Sprite defaultCrosshair, clickCrosshair;
    [SerializeField] private Image crosshairImage;
    [SerializeField] private LayerMask layerMask;
    private Sprite currentCrosshairSprite;
    private RaycastHit hit;
    public bool canBeUsed;
    private bool isLookingAtInteractable;
    private Color defaultColor, fadeColor, currentColor;

    private void Start()
    {
        defaultColor = crosshairImage.color;
        fadeColor = new Color(0, 0, 0, 0);
        ServiceLocator.playerInteractionInstance = this;
    }

    void Update()
    {
        if (crosshairImage.color != currentColor) crosshairImage.color = Color.Lerp(crosshairImage.color, currentColor, 0.1f * Time.deltaTime * 50);
        if (crosshairImage.sprite != currentCrosshairSprite)
        {
            crosshairImage.sprite = currentCrosshairSprite;
        }

        if (canBeUsed)
        {
            currentCrosshairSprite = clickCrosshair;
            CheckInteractableHit();
        }
        else
        {
            currentCrosshairSprite = defaultCrosshair;
        }
    }

    
    // INTERACTION RELATED METHODS


    //This checks if the player currently looks at an interactable gameObject
    private void CheckInteractableHit()
    {
        //setting the variables to the variables that would be when the game has to fade out. It cant be done in the if statement
        //due to the sprite not resetting back to normal after looking back from an interactable
        currentColor = fadeColor;
        currentCrosshairSprite = defaultCrosshair;
        isLookingAtInteractable = false;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 5, layerMask))
        {
            if (hit.transform.gameObject.CompareTag("FadeHUD"))
            {
                return;
            }

            Interactable cachedInteractable = hit.transform.gameObject.GetComponent<Interactable>();
            if(cachedInteractable != null)
            {
                Interact(cachedInteractable);
            }
            else
            {
                Debug.LogError("The Gameobject(" + hit.transform.gameObject + ") has no Instance of 'Interactable' attached, altough it is in the interacable layer");
            }
        }

        currentColor = defaultColor;
        if (isLookingAtInteractable)
        {
            currentCrosshairSprite = clickCrosshair;
        }
        else
        {
            currentCrosshairSprite = defaultCrosshair;
        }

    }

    //This executed the given instances interact event on input
    private void Interact(Interactable instance)
    {
        isLookingAtInteractable = true;
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            instance.Interact();
        }
    }


    //UI RELATED METHODS


    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawRay(transform.position, transform.forward * 5);       //drawing the same inputray as in CheckInteractableHit()
    }
}
