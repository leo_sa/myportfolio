using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    protected delegate void InteractEvent();
    protected InteractEvent InteractEventInstance;     //Delegate to subscribe to if inherited from this class

   public void Interact()
   {
       InteractEventInstance();
   }
}
