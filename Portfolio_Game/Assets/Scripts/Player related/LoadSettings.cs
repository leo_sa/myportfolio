using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadSettings : MonoBehaviour
{
    void Start()
    {
        LoadGRaphics();
        LoadAudio();
    }

    [SerializeField] private GraphicalOptions toLoadGraphics;
    private void LoadGRaphics()
    {
        try
        {
            toLoadGraphics = SaveToJsonGeneric<GraphicalOptions>.LoadData(toLoadGraphics, "video");

            //Resolution is saved so no manual set required

            Screen.fullScreen = toLoadGraphics.fullScreen;
            QualitySettings.vSyncCount = toLoadGraphics.vsyncamount;
        }
        catch
        {
            //No Video Options detected
        }
    }

    [SerializeField] private AudioOptions toLoadAudio;
    private void LoadAudio()
    {
        try
        {
            toLoadAudio = SaveToJsonGeneric<AudioOptions>.LoadData(toLoadAudio, "audio");
            AudioListener.volume = toLoadAudio.audioMasterVolume;
        }
        catch
        {
            //NO AUDIO OPTIONS DETECTED
        }
    }

}
