using UnityEngine;

public struct MapInputEvent
{
    //Index here suggests the input to change in GetInput
    public readonly int index;
    public readonly KeyCode key;

    public MapInputEvent(int i, KeyCode keyCode)
    {
        index = i;
        key = keyCode;
    }
}