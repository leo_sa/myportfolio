using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUIManager : MonoBehaviour
{
    private static GameUIManager instance;
    
    void Start()
    {
        ServiceLocator.managerUI = this;            //This should aways have only one instance
        //ensuring only a single instance
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }

        //Disabling cursor in the beginning
        ChangeCursorStatus(false);
    }

    
    void Update()
    {
        //Hardcoding ESC due to ESC being the standart to pause and there is currently no need to change this i think
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ChangePauseMenuStatus();
        }
    }

    [SerializeField] private GameObject pauseMenu, pauseMenuChild;
    [SerializeField] private GameObject[] additionalWindows;
    [SerializeField] private GameObject hud;

    public void ChangePauseMenuStatus()
    {
        //timescale will be set to 0 when enabling pause and to 1 if disabling
        float? timeMod = !pauseMenu.activeInHierarchy ? 0 : 1;

        Time.timeScale = timeMod.Value;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;

        if (pauseMenu.activeInHierarchy)   //Code when exiting pause
        {           
            //Disabling all additional windows when leaving pauseMenu
            for (int i = 0; i < additionalWindows.Length; i++)
            {
                additionalWindows[i].SetActive(false);
            }
        }
        else //Code when enabling pause    
        {
                  
            // and ensuring childs activation
            pauseMenuChild.SetActive(true);
        }
        pauseMenu.SetActive(!pauseMenu.activeInHierarchy);
        ChangeCursorStatus(pauseMenu.activeInHierarchy);
    }

    //if freeCursor lockmode will be none else cursor will be locked (and invisible)
    public void ChangeCursorStatus(bool freeCursor)
    {
        hud.SetActive(!freeCursor);
        CursorLockMode? lockmode = freeCursor ? CursorLockMode.None : CursorLockMode.Locked;
        Cursor.lockState = lockmode.Value;
        Cursor.visible = freeCursor;
    }
}
