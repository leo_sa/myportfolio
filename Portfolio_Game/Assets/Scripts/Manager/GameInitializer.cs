using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInitializer : MonoBehaviour
{
    [SerializeField] private RenderTexture renderTexture;
  
    void Start()
    {
        //The rendertexture has to be released at start to not save the last seen video-frame
        renderTexture.Release();
    }
}
