using System.Collections;
using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine;

public class VideoManager : MonoBehaviour
{
    [SerializeField] private RenderTexture renderTex;
    [SerializeField] private VideoPlayer videoPlayer;

    //Plays video according to index
    public void PlayVideo(VideoClip clip)
    {
        //videoPlayer.clip = clip;
        videoPlayer.Play();
    }

    public void StopVideo()
    {
        videoPlayer.Stop();
        renderTex.Release();
    }

    public IEnumerator PlayWithDelay(VideoClip toPlay, string url)
    {
        StopVideo();
        videoPlayer.url = System.IO.Path.Combine(Application.streamingAssetsPath, url + ".mp4");
        yield return new WaitForSeconds(0.1f);      //having a very small delay gives the user to press RMB in time
        PlayVideo(toPlay);
    }
}
