using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ServiceLocator
{
    public static GameUIManager managerUI;
    public static FirstPersonCamera fpsCamInstance;
    public static PlayerInteraction playerInteractionInstance;
}
