using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InputMapper : MonoBehaviour
{
    [SerializeField] private InputMappingScOb inputScOb;

    [SerializeField] private TextMeshProUGUI[] inputTexts;

    private readonly Array keyCodes = Enum.GetValues(typeof(KeyCode));

    private bool listens;
    private bool wasEnabled;

    private int index;

    private void OnEnable()
    {
        //Only loading controls once when started "freshly"
        if (!wasEnabled)
        {
            LoadControls();
        }
    }

    private void OnDisable()
    {
        if (listens)
        {
            CancelListening();
        }
        SaveControls();
        wasEnabled = true;       
    }

    // if ESC is pressed listening stops
    private void Update()
    {
        if(listens && Input.GetKeyDown(KeyCode.Escape))
        {
            CancelListening();
        }

        if (listens && Input.anyKeyDown && !Input.GetKeyDown(KeyCode.Escape))
        {      
            ListenForInput();
        }
    }

    //Index will be set to corresponding value set in the UI button
    public void StartListening(int i)
    {
        index = i;
        inputTexts[i].text = "_";
        listens = true;
    }

   private void ListenForInput()
   {
       foreach(KeyCode keyCode in keyCodes)
       {
            //Keycode will be assigned [Method in GetInput is subscribed to the messageDelegate]
            if (Input.GetKey(keyCode))
            {
                inputTexts[index].text = keyCode.ToString();
                MessageDelegate.Raise(new MapInputEvent(index, keyCode));
                inputScOb.savedcontrols[index] = keyCode;
                listens = false;
                return;
            }
       }

       //This thingy here is only executed when the loop above is done incorrectly
        Debug.Log("Error while getting Input");
        listens = false;
   }

    //changing back
    private void CancelListening()
    {
        inputTexts[index].text = inputScOb.savedcontrols[index].ToString();
        listens = false;
    }

    public void SaveControls()
    {
        SaveToJsonGeneric<InputMappingScOb>.SaveThisToJson(inputScOb, "inputmap");
    }

    //loading saved Controls
    public void LoadControls()
    {
        try
        {
        }
        catch
        {
            Debug.Log("No Input Data detected");
            //else will be first time setting it. Maybe a text a l� "no input data detected"
        }
        inputScOb = SaveToJsonGeneric<InputMappingScOb>.LoadData(inputScOb, "inputmap");

        for (int i = 0; i < inputTexts.Length; i++)
        {
            // the catch here will be executed when i exceeds the saved input, meaning, that new input was registered
            try
            {
                inputTexts[i].text = inputScOb.savedcontrols[i].ToString();
            }
            catch
            {
                //In this case the defaults will be given
                inputTexts[i].text = inputScOb.defaultControls[i].ToString();
            }

        }

    }
}
