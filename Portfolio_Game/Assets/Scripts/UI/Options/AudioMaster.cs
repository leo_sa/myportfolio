using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioMaster : MonoBehaviour
{
    [SerializeField] private AudioOptions options;
    [SerializeField] private Slider masterSlider;

    private void Start()
    {
        LoadOptions();    
    }

    private void OnDisable()
    {
        SaveOptions();
    }

    public void ChangeListenerVolume()
    {
        AudioListener.volume = masterSlider.value;
        options.audioMasterVolume = masterSlider.value;
    }

    private void LoadOptions()
    {
        try
        {
            options = SaveToJsonGeneric<AudioOptions>.LoadData(options, "audio");
            AudioListener.volume = options.audioMasterVolume;
            masterSlider.value = options.audioMasterVolume;
        }
        catch
        {
            //NO AUDIO OPTIONS DETECTED
        }
    }

    private void SaveOptions()
    {
        SaveToJsonGeneric<AudioOptions>.SaveThisToJson(options, "audio");
    }
}
