using UnityEngine;
using UnityEngine.UI;

public class CameraSensitivitySlider : MonoBehaviour
{
    [SerializeField] private Slider slider;

    [SerializeField] private FirstPersonCamera fpsCam;


    private void Start()
    {
        NullValueAssignment();
    }

    //If some exposed variables dont have a value set, this method will "save" them
    private void NullValueAssignment()
    {
        if (fpsCam == null)
        {
            fpsCam = FindObjectOfType<FirstPersonCamera>(); //because this script should ALWAYS have a single instance this is possible
        }
        if(slider == null)
        {
            slider = GetComponent<Slider>();
        }
    }

    public void ChangeSliderSensitivity()
    {
        fpsCam.sensitivity = slider.value;
    }
}
