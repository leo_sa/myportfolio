using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Page : MonoBehaviour
{
    [SerializeField] private Page prevPage, nextPage;
    [SerializeField] private RectTransform rectTransform;
    public Vector3 pushPosition;      //Position to Lerp to
    [HideInInspector] public Vector3 targetPos;        //the default position
    [HideInInspector] public Vector3 originalPos;        //the default position

    private void Start()
    {
        originalPos = rectTransform.localPosition;
        targetPos = originalPos;

        if (Chance(50))
        {
            pushPosition.x *= -1;     //Randomizing if page will go to the right or left
        }     
    }

    //used to get a random chance
    private bool Chance(int chance)
    {
        int n = Random.Range(0, 100);

        if (n <= chance) return true;
        else return false;
    }

    private Vector3 interpolationVector;        //used to interpolate values
    private void Update()
    {
        if(rectTransform.localPosition != targetPos)
        {
            rectTransform.localPosition = Vector3.Lerp(rectTransform.localPosition, targetPos, 0.2f * Time.unscaledDeltaTime * 50);
        }
    }

    public void PrevPage()
    {
        prevPage.targetPos = prevPage.originalPos;
    }

    public void NextPage()
    {
        targetPos = pushPosition;
    }

}
