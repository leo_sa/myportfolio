using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAndChangeText : MonoBehaviour
{
    [SerializeField, Range(0.01f, 1f), Tooltip("The amount of degrees changed per loop")] private float rotationIntensity;
    [SerializeField, Range(0.00001f, 0.01f), Tooltip("The Wait Time in the while-loop")] private float rotationSpeed = 0.001f;
    [SerializeField] private RectTransform rectTransform;

    private void Start()
    {
        //This value should always be positive
        rotationIntensity = Mathf.Abs(rotationIntensity);
    }

    public IEnumerator RotateAround()
    {
        //Creating another rotationVector to deal with Quaternions correctly
        Vector3 cachedRotation = new Vector3(rectTransform.localEulerAngles.x, rectTransform.localEulerAngles.y, rectTransform.localEulerAngles.z);

        //if the current rotation.x value is under -100 it will be around -450 so 90 should be targeted and vice cersa
        if(rectTransform.localEulerAngles.x <= -100)
        {
            while (cachedRotation.x <= -90)
            {
                cachedRotation.x += rotationIntensity;
                rectTransform.localEulerAngles = cachedRotation;
                yield return new WaitForSeconds(rotationSpeed);
            }
        }
        else
        {
            while (cachedRotation.x >= -450)
            {
                cachedRotation.x -= rotationIntensity;
                rectTransform.localEulerAngles = cachedRotation;
                yield return new WaitForSeconds(rotationSpeed);
            }
        }
       //TEST HIS OUT :�
    }
}
