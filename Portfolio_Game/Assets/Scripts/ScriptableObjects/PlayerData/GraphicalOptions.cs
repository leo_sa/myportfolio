using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/GraphicOptions", order = 2)]
public class GraphicalOptions : ScriptableObject
{
    public Resolution currentResolution;
    public int currentDropDownIndex, vsyncamount;
    public bool fullScreen = true, vsync = false;
}
