using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/AudioOptions", order = 3), System.Serializable]
public class AudioOptions : ScriptableObject
{
    public float audioMasterVolume;
}
