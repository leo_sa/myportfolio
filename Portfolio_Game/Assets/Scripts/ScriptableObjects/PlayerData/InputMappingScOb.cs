using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/InputMap", order = 1), System.Serializable]
public class InputMappingScOb : ScriptableObject
{
    public KeyCode[] savedcontrols;

    //default Controls go here
    public readonly KeyCode[] defaultControls = { KeyCode.Space, KeyCode.LeftShift };
}
