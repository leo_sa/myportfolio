using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayButtonMaster : MonoBehaviour
{
   [SerializeField] private float difference;
   private GameObject[] possibleButtons;
   [SerializeField, Tooltip("The Amount of playbuttons")] private int maxIndex;
   private int index;       //describes the current play index
   private Vector3 targetPosition;
   private Vector3 translationVector;  //an additional vector is used to use the interpolation properly

    private void Start()
    {
        targetPosition = transform.localPosition;
        translationVector = targetPosition;
    }

    public void ChangePlayButton(bool nextOrPrev)
    {
        float? toAdd = nextOrPrev ? difference : -difference;

        //Dont go over 0 and MaxIndex - 1(index counting starts at 0)
        if (index == maxIndex - 1 && toAdd > 0) return;
        else if (index == 0 && toAdd < 0) return;

        targetPosition.z += toAdd.Value;
        index += (int)Mathf.Sign(toAdd.Value);
    }


    private void Update()
    {
        if(transform.localPosition != targetPosition)
        {
            translationVector = Vector3.Lerp(translationVector, (targetPosition - transform.localPosition) / 12, 0.075f);

            transform.localPosition += translationVector;
        }
    }
}
