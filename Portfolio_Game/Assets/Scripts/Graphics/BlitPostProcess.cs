using UnityEngine;

[ExecuteInEditMode]
public class BlitPostProcess : MonoBehaviour
{
    [SerializeField] private Material mat;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Debug.Log("test");
        Graphics.Blit(source, destination, mat);
    }
}
