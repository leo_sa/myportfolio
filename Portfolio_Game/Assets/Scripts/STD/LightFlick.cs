using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlick : MonoBehaviour2
{
    [SerializeField] private Light light;
    float inten;
    public float intensity;
    float start;

    [Range(0, 1)]
    public float lerp;

    void Start()
    {
        if (!light) light = GetComponent<Light>();
        start = light.intensity;
    }


    void Update()
    {
        inten = Random.Range(start - intensity, start + intensity);

        light.intensity = Mathf.Lerp(light.intensity, inten, lerp * TimeMultiplication());
        light.range = Mathf.Lerp(light.intensity, inten, lerp * TimeMultiplication());

    }
}