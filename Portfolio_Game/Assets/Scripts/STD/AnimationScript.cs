﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class AnimationScript : MonoBehaviour
    {
        [SerializeField]
        private float speedX, speedY, speedZ, LERP_rangeX, LERP_rangeY, LER_PrangeZ, rotationSpeedX, rotationSpeedY, rotationSpeedZ;
        [SerializeField, Range(0, 1)]
        private float lerpSpeed = 0.2f, LERP_inaccuracy = 0.1f, rotationReset_accuracy = 0.3f;

        private float internSpeedX, internspeedY, internSpeedZ, internLerpX, internLerpY, internLerpZ;
        private float internRotX, internRotY, internRotZ;

        private Vector3 originalPosition;

        private bool lerpTo;

        private void Start()
        {
            originalPosition = transform.position;
        }


        private void Update()
        {
            LERP_Calculations();
            Rotation_Calculations();

            transform.position = Vector3.Lerp(transform.position, new Vector3(internLerpX, internLerpY, internLerpZ), lerpSpeed) + (new Vector3(internSpeedX, internspeedY, internSpeedZ));

            internSpeedX += speedX * Time.deltaTime;
            internspeedY += speedY * Time.deltaTime;
            internSpeedZ += speedZ * Time.deltaTime;
        }

        private void LERP_Calculations()
        {
            if (Vector3.Distance(transform.position, new Vector3(internLerpX, internLerpY, internLerpZ)) < LERP_inaccuracy)
            {
                if (lerpTo) lerpTo = false;
                else lerpTo = true;
            }

            if (lerpTo)
            {
                internLerpX = originalPosition.x + LERP_rangeX;
                internLerpY = originalPosition.y + LERP_rangeY;
                internLerpZ = originalPosition.z + LER_PrangeZ;
            }
            else
            {
                internLerpX = originalPosition.x - LERP_rangeX;
                internLerpY = originalPosition.y - LERP_rangeY;
                internLerpZ = originalPosition.z - LER_PrangeZ;
            }
        }

        private void Rotation_Calculations()
        {
            if (internRotX % 360 <= rotationReset_accuracy) internRotX = 0;
            if (internRotY % 360 <= rotationReset_accuracy) internRotY = 0;
            if (internRotZ % 360 <= rotationReset_accuracy) internRotZ = 0;

            internRotX += rotationSpeedX * Time.deltaTime;
            internRotY += rotationSpeedY * Time.deltaTime;
            internRotZ += rotationSpeedZ * Time.deltaTime;

            transform.rotation = Quaternion.Euler(internRotX, internRotY, internRotZ);
        }
    }

}
