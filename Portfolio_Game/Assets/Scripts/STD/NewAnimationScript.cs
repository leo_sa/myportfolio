using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewAnimationScript : MonoBehaviour2
{
    [SerializeField] private Vector3 rotationIntensity, newRotationOffset;
    [SerializeField] private float speed = 1, additionalStartTime;
    private Vector3 offset;
    [SerializeField] private Vector2 randomVector;
    [SerializeField] private bool useSinTime;
    [SerializeField] private bool randomizeSpeed, randomizeRotationVector, randomizeStartTime;

    private void Start()
    {
        Randomizer();
        offset = transform.localEulerAngles;
    }

    void Randomizer()
    {
        if (randomizeSpeed)
        {
            speed *= Random.Range(randomVector.x, randomVector.y);
        }

        if (randomizeRotationVector)
        {
            rotationIntensity *= Random.Range(randomVector.x, randomVector.y);
        }

        if (randomizeStartTime)
        {
            additionalStartTime *= Random.Range(randomVector.x, randomVector.y);
        }
    }

    private void FixedUpdate()
    {
        if (useSinTime)
        {
            transform.localRotation = Quaternion.Euler((rotationIntensity + offset) * SinTime(speed + additionalStartTime) * TimeMultiplication());
        }
        else
        {
            Vector3 useVector = new Vector3(rotationIntensity.x + offset.x * Time.time, rotationIntensity.y + offset.y * Time.time, rotationIntensity.z + offset.z * Time.time);
            transform.localRotation = Quaternion.Euler((rotationIntensity) * (Time.time + additionalStartTime) + newRotationOffset * TimeMultiplication());
        }
    }

}
