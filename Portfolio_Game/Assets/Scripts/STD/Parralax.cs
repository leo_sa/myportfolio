﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class Parralax : MonoBehaviour
    {
        [SerializeField, Range(0, 1)]
        float parralaxStrength;
        float length, startPosition;

        Transform cam;

        [SerializeField] bool alsoMove;
        [SerializeField] float speed;
        float movedDistance;


        void Start()
        {
            cam = Camera.main.gameObject.transform;
            startPosition = transform.position.x;
            length = GetComponent<SpriteRenderer>().bounds.size.x;
        }

        void Update()
        {
            float boundsCheck = cam.position.x * (1 - parralaxStrength);
            float distance = (cam.position.x * parralaxStrength);            

            if(alsoMove) transform.position = new Vector3(startPosition + distance + movedDistance, transform.position.y, transform.position.z);
            else transform.position = new Vector3(startPosition + distance, transform.position.y, transform.position.z);

            if (boundsCheck > startPosition + length)
            {
                startPosition += length * 2;
                movedDistance = 0;
            }
            else if (boundsCheck < startPosition - length)
            {
                startPosition -= length * 2;
                movedDistance = 0;
            }
        }
    }
