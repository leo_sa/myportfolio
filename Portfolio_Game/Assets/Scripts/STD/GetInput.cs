using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetInput : MonoBehaviour2
{
    [SerializeField] private InputMappingScOb inputSCOB;
    //List of individual input usages at the end of script
    private KeyCode[] indivInputs = { KeyCode.Space, KeyCode.LeftShift };

    protected float inputX, inputY, inputXRaw, inputYRaw;
    protected float mx, my;
    protected Vector2 inputVectorRaw, inputVector;

    private void OnEnable()
    {
        MessageDelegate<MapInputEvent>.Add(ChangeInputMap);
        LoadInput();
    }

    private void OnDisable()
    {
        MessageDelegate<MapInputEvent>.Remove(ChangeInputMap);
    }

    private void LoadInput()
    {
        //loading stuff from saved data
        try
        {
          inputSCOB = SaveToJsonGeneric<InputMappingScOb>.LoadData(inputSCOB, "inputmap");
        }
        catch
        {
            //no input data detected
        }
        indivInputs = inputSCOB.savedcontrols;
    }

    //this is just an individual key change executed from the InputMapper
    public void ChangeInputMap(MapInputEvent ev)
    {
        indivInputs[ev.index] = ev.key;
        string playerPrefsKey = "KeyIndex" + ev.index;
    }

    protected void GetCoreInput()
    {
        inputXRaw = Input.GetAxisRaw("Horizontal");
        inputX = Input.GetAxis("Horizontal");

        inputYRaw = Input.GetAxisRaw("Vertical");
        inputY = Input.GetAxis("Vertical");

        inputVectorRaw = new Vector2(inputXRaw, inputYRaw);
        inputVector = new Vector2(inputX, inputY);
    }

    protected void GetMouseInput()
    {
        mx = Input.GetAxis("Mouse X");
        my = Input.GetAxis("Mouse Y");
    }

    protected float internSprint = 1;
    private float sprintMultiplier = 2;

    protected void GetSprintInput()
    {
        
        if (Input.GetKey(indivInputs[1]) && Input.GetAxisRaw("Vertical") == 1)
        {
            internSprint = SmoothLerpValue(internSprint, sprintMultiplier, 0.1f, 0.8f);
        }
        else 
        {
            internSprint = SmoothLerpValue(internSprint, 1, 0.1f, 0.8f);
        }
    }

    protected bool jump, jumpDown;
    protected void GetKeyInput()
    {
        jump = Input.GetKey(indivInputs[0]);
        jumpDown = Input.GetKeyDown(indivInputs[0]);
    }


    //List of Key Inputs:
    //0: Jump
    //1: Sprint

}
