using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SaveToJsonGeneric<T>
{
   public static void SaveThisToJson(T toSave, string path)
   {
        string filePath = Path.Combine(Application.persistentDataPath, (path + ".json"));
        string toSaveString = JsonUtility.ToJson(toSave);
        File.WriteAllText(filePath, toSaveString);
   }

    //CURRENTLY NOT USABLE WITH MONOBEHAIVIOUR AND SCROB
    public static string GetSavedJsonString(T toLoad, string path)
    {
        string filePath = Path.Combine(Application.persistentDataPath, (path + ".json"));

        if (File.Exists(filePath))
        {
            string cachedSaveString = File.ReadAllText(filePath);
            return cachedSaveString;
        }
        else
        {
            Debug.Log("No File found here");
            return "ERROR";
        }

    }

    public static T LoadData(T input, string path)
    {
        string filePath = Path.Combine(Application.persistentDataPath, (path + ".json"));

        if (File.Exists(filePath))
        {
            string cachedSaveString = File.ReadAllText(filePath);
           
            T cachedgeneric = input;

            //deciding how to read the data properly
            if(input is MonoBehaviour || input is ScriptableObject)
            {
                JsonUtility.FromJsonOverwrite(cachedSaveString, cachedgeneric);
            }
            else
            {
              cachedgeneric = JsonUtility.FromJson<T>(cachedSaveString);
            }

            return cachedgeneric;
        }
        else
        {
            Debug.LogError("Tried to load a non existend file at: " + filePath);
            return input;
        }
    }
}
