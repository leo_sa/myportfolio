﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShowFPS : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text, avgText;
    float stuff;
    bool useConsole;
    int passedFrames;
    private float avgFPS;

    private void Start()
    {
        useConsole = !text;
    }

    void Update()
    {
        passedFrames++;
        stuff += (Time.unscaledDeltaTime - stuff) * 0.1f;
        float fps = 1.0f / stuff;
        avgFPS += (fps - avgFPS) / passedFrames;

        if (useConsole)
        {
           Debug.Log(fps);
        }
        else
        {
            text.text = (int)fps + " FPS";
            avgText.text = (int)avgFPS + " Average";
        }

        if(passedFrames > 100000000)
        {
            Debug.Log("Passed frames reset");
            passedFrames = 0;
            avgFPS = 0;
        }
    }
}
