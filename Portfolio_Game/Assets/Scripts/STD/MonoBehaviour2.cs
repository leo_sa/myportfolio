﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoBehaviour2 : MonoBehaviour         //created 02.06.2020
{
    private int valueToChange;
    private bool changeTicket = true;           //used for "onValueChanged"



    public void Log(object tolog)       //wrapped Debug.log method  
    {
        Debug.Log(tolog);
    }


    //A Method to play an audiosource with more control and reduced repetition
    public void MyPlay(AudioSource audioSource, AudioClip clip, float originalPitch, float pitchModification, float delay, bool loop, bool timeModification)
    {
        audioSource.clip = clip;
        audioSource.loop = loop;

        if (timeModification)
        {
            audioSource.pitch = originalPitch + Random.Range(-pitchModification, pitchModification) * Time.timeScale;
        }
        else
        {
            audioSource.pitch = originalPitch + Random.Range(-pitchModification, pitchModification);
        }


        audioSource.PlayDelayed(delay);
    }


    //A Method to play an audiosource easier, when multiple audiosources are available
    public void MyPlay(AudioSource audioSource, AudioClip clip)
    {
        audioSource.clip = clip;
        audioSource.pitch = (Random.Range(0.9f, 1.1f)) * Time.timeScale;
        audioSource.Play();
    }


    // A simple Chance for if statements
    public bool Chance(int chance)
    {
        int n = Random.Range(0, 100);

        if (n <= chance) return true;
        else return false;
    }




    //for if statements, true for one frame when value is changed
    public bool OnIntegerValueChanged(int value)            //can only be used once per script
    {
        if (changeTicket)
        {
            valueToChange = value;
            changeTicket = false;
        }

        if (valueToChange != value)
        {
            changeTicket = true;
            return true;
        }
        else
        {
            return false;
        }

    }

    //Lerp but float-inaccuracy is taken in
    public Vector3 SmoothLerp(Vector3 from, Vector3 target, float buffer, float speed)
    {

        if (Vector3.Distance(from, target) > buffer)
        {
            from = Vector3.Lerp(from, target, speed * 50 * Time.deltaTime);
        }
        else
        {
            from = target;
        }

        return from;
    }

    //Lerp but float-inaccuracy is taken in
    public float SmoothLerpValue(float from, float target, float buffer, float speed)
    {

        if (Mathf.Abs(from - target) > buffer)
        {
            from = Mathf.Lerp(from, target, speed * 50 * Time.deltaTime);
        }
        else
        {
            from = target;
        }

        return from;
    }

    public float WobbleLerp(float from, float target, float speed)
    {
        float x = from + Mathf.Lerp(0, (target - from), speed * TimeMultiplication());
        return x;
    }

    //Flip an Object based on an index
    public void Flip(Vector3 from, SpriteRenderer renderer, int flipIndex)      //flipIndex = 0 Null, 1 Horizontal, 2 Vertical, 3 both
    {
        if ((transform.position.x - from.x) < 0)
        {
            if (renderer != null)
            {
                switch (flipIndex)
                {
                    case 1:
                        renderer.flipX = false;
                        break;

                    case 2:
                        renderer.flipY = false;
                        break;

                    case 3:
                        renderer.flipX = false;
                        renderer.flipY = false;
                        break;
                }

            }
            else
            {
                switch (flipIndex)
                {
                    case 1:
                        transform.localScale = new Vector3(1, 1, 1);
                        break;

                    case 2:
                        transform.localScale = new Vector3(1, 1, 1);
                        break;

                    case 3:
                        transform.localScale = new Vector3(1, 1, 1);
                        break;
                }
            }
        }
        else
        {
            if (renderer != null)
            {
                switch (flipIndex)
                {
                    case 1:
                        renderer.flipX = true;
                        break;

                    case 2:
                        renderer.flipY = true;
                        break;

                    case 3:
                        renderer.flipX = true;
                        renderer.flipY = true;
                        break;
                }

            }
            else
            {
                switch (flipIndex)
                {
                    case 1:
                        transform.localScale = new Vector3(-1, 1, 1);
                        break;

                    case 2:
                        transform.localScale = new Vector3(1, -1, 1);
                        break;

                    case 3:
                        transform.localScale = new Vector3(-1, -1, 1);
                        break;
                }
            }
        }
    }

    //returns all children of the object
    public Transform[] GetAllChildren()
    {
        List<Transform> childList = new List<Transform>();

        for (int i = 0; i < transform.childCount; i++)
        {
            childList.Add(transform.GetChild(i));
        }

        return childList.ToArray();
    }


    //returns a random integer
    public int RandomNumber(int length)
    {
        int num;
        num = Random.Range(0, length);
        return num;
    }


    //Changes the hue of an color
    public Color ChangeHue(Color OriginalColorRGB, float H)
    {
        float localH, localS, localV;
        Color.RGBToHSV(OriginalColorRGB, out localH, out localS, out localV);

        localH = H;

        return Color.HSVToRGB(localH, localS, localV);

    }

    //rotate towards an object
    public void RotateTowards(Transform input, Vector3 towards, float offset)
    {
         float rotZ;

         Vector3 difference = towards - transform.position;
         rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;

        input.rotation = Quaternion.Euler(0f, 0f, rotZ + offset);
    }

    //rotate towards an object, smoothed
    public void RotateTowardsLerp(Vector3 towards, float offset, float lerpSpeed)
    {
        float rotZ;

        Vector3 difference = towards - transform.position;
        rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0f, 0f, rotZ + offset), lerpSpeed * TimeMultiplication());
    }

    //set framerate independent
    public float TimeMultiplication()
    {
        float returnValue = Time.deltaTime * 100;
        return returnValue;
    }

    //change the timescale smoothly
    public void SetTime(float mutliplicator)
    {
        Time.timeScale = mutliplicator;
        Time.fixedDeltaTime = 0.02f * mutliplicator;
    }


    //Returns continious Sinus-Wave multiplied with the argument
    public float SinTime(float multiplier)
    {
        return Mathf.Sin(Time.time * multiplier);
    }

    //Is a value equal to another with a buffer of x?
    public bool EqualsAround(float inValue, float targetValue, float buffer)
    {
        if(Mathf.Abs(inValue - targetValue) < buffer)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}


