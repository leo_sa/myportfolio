using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageDelegate<T>
{
    public static readonly MessageDelegate<T> Instance = new MessageDelegate<T>();

    private Action<T> delegateAction = delegate { };

    private void InternSet(Action<T> handler)
    {
        delegateAction = handler;
    }

    public static void Set(Action<T> handler)
    {
        Instance.InternSet(handler);
    }

    private void InternAdd(Action<T> handler)
    {
        delegateAction += handler;
    }

    public static void Add(Action<T> handler)
    {
        Instance.InternAdd(handler);
    }

    private void InternalRemove(Action<T> handler)
    {
        delegateAction -= handler;
    }

    public static void Remove(Action<T> handler)
    {
        Instance.InternalRemove(handler);
    }

    private void InternalRaise(T genericEvent)
    {
        delegateAction(genericEvent);
    }

    public static void Raise(T genericEvent)
    {
        Instance.InternalRaise(genericEvent);
    }
}

public static class MessageDelegate
{
    public static void Raise<T>(T genericEvent)
    {
        MessageDelegate<T>.Raise(genericEvent);
    }
}
