﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class FlipXY : MonoBehaviour
    {
        [SerializeField]
        bool x, y;                          //FlipSpriteX or Y?

        Transform followObject;
        [HideInInspector]
        public SpriteRenderer spriteRenderer;           

        [SerializeField]
        string follow;

        [HideInInspector]
        public bool canFlip, isFlipped;            
        public bool followMouse;                    //Is the Mouse the "FlipSetter"?

        [SerializeField]
        bool useRotationInsteadOfSprites, useScale;
        
        Camera cam;

        void Start()
        {
            canFlip = true;
            if (!followMouse) followObject = GameObject.Find(follow).transform;
            spriteRenderer = GetComponent<SpriteRenderer>();
            cam = Camera.main;
        }

        void Update()
        {
            if (!useScale)
            {
                if (!useRotationInsteadOfSprites)
                {
                    if (canFlip)
                    {
                        if (!followMouse)
                        {
                            if ((transform.position.x - followObject.position.x) <= 0)
                            {
                                if (x) spriteRenderer.flipX = false;
                                if (y) spriteRenderer.flipY = false;

                                isFlipped = true;
                            }
                            else
                            {
                                if (x) spriteRenderer.flipX = true;
                                if (y) spriteRenderer.flipY = true;

                                isFlipped = false;
                            }
                        }
                        else
                        {

                            
                                if ((transform.position.x - cam.ScreenToWorldPoint(Input.mousePosition).x <= 0))
                                {
                                    if (x) spriteRenderer.flipX = false;
                                    if (y) spriteRenderer.flipY = false;

                                    isFlipped = false;
                                }
                                else
                                {
                                    if (x) spriteRenderer.flipX = true;
                                    if (y) spriteRenderer.flipY = true;

                                    isFlipped = true;
                                }
                            

                        }

                    }

                }
                else
                {
                    if (!followMouse)
                    {
                        if ((transform.position.x - followObject.position.x) <= 0)
                        {
                            if (x) transform.transform.rotation = Quaternion.Euler(0, transform.eulerAngles.y, transform.eulerAngles.z);
                            if (y) transform.rotation = Quaternion.Euler(transform.eulerAngles.x, 0, transform.eulerAngles.z);

                            isFlipped = false;
                        }
                        else
                        {
                            if (x) transform.transform.rotation = Quaternion.Euler(-180, transform.eulerAngles.y, transform.eulerAngles.z);
                            if (y) transform.rotation = Quaternion.Euler(transform.eulerAngles.x, -180, transform.eulerAngles.z);

                            isFlipped = true;
                        }
                    }
                    else
                    {
                      
                            if ((transform.position.x - cam.ScreenToWorldPoint(Input.mousePosition).x <= 0))
                            {
                                if (x) transform.rotation = Quaternion.Euler(0, transform.eulerAngles.y, transform.eulerAngles.z);
                                if (y) transform.rotation = Quaternion.Euler(transform.eulerAngles.x, 0, transform.eulerAngles.z);

                                isFlipped = false;
                            }
                            else
                            {
                                if (x) transform.rotation = Quaternion.Euler(-180, transform.eulerAngles.y, transform.eulerAngles.z);
                                if (y) transform.rotation = Quaternion.Euler(transform.eulerAngles.x, -180, transform.eulerAngles.z);

                                isFlipped = true;
                            }
                        

                    }
                }
            }
            else
            {
                if (!followMouse)
                {
                    if ((transform.position.x - followObject.position.x) <= 0)
                    {
                        if (x) transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);
                        if (y) transform.localScale = new Vector3(transform.localScale.x, 1, transform.localScale.z);

                        isFlipped = false;
                    }
                    else
                    {
                        if (x) transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);
                        if (y) transform.localScale = new Vector3(transform.localScale.x, -1, transform.localScale.z);

                        isFlipped = true;
                    }
                }
                else
                {
                   
                        if ((transform.position.x - cam.ScreenToWorldPoint(Input.mousePosition).x <= 0))
                        {
                            if (x) transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);
                            if (y) transform.localScale = new Vector3(transform.localScale.x, 1, transform.localScale.z);

                            isFlipped = false;
                        }
                        else
                        {
                            if (x) transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);
                            if (y) transform.localScale = new Vector3(transform.localScale.x, -1, transform.localScale.z);

                            isFlipped = true;
                        }
                    

                }
            }
            

        }
    }


