using UnityEngine;
using UnityEngine.Video;

public class PlayVideoButtonEvent : CustomButton
{
    [SerializeField] private VideoManager videoManager;
    [SerializeField] private VideoClip clipToPlay;

    private void Awake()
    {
        eventInstance += PlayVideo;
    }

    private void PlayVideo()
    {
        StartCoroutine(videoManager.PlayWithDelay(clipToPlay, clipToPlay.name));
    }
}
