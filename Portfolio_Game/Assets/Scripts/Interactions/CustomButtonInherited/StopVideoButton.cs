using UnityEngine;

public class StopVideoButton : CustomButton
{
    [SerializeField] private VideoManager videoManager;
   
    void Awake()
    {
        eventInstance += StopVideo;
    }

    private void StopVideo()
    {
        videoManager.StopVideo();
    }
}
