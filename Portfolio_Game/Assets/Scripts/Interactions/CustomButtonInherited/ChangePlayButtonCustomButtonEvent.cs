using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePlayButtonCustomButtonEvent : CustomButton
{
    [SerializeField, Tooltip("true will be next, false will be previous")] private bool nextPage;
    [SerializeField] private PlayButtonMaster buttonMaster;

    private void Awake()
    {
        eventInstance += ChangePlayButton;
    }

    private void ChangePlayButton()
    {
        buttonMaster.ChangePlayButton(nextPage);
    }
}
