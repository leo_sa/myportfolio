using UnityEngine;

public class CopyStringToClipboard : CustomButton
{
    [SerializeField] private string toCopy;
    [SerializeField] private Sprite spriteAfterPress;

    private void Awake()
    {
        eventInstance += CopyStringToClipBoard;
        eventInstance += ChangeSprite;
    }

    private void CopyStringToClipBoard()
    {
        GUIUtility.systemCopyBuffer = toCopy;
    }

    private void ChangeSprite()
    {
        spriteRenderer.sprite = spriteAfterPress;
    }
}