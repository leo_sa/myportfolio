using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomButton : MonoBehaviour
{
    protected delegate void CustomButtonEvent();
    protected CustomButtonEvent eventInstance;
    private bool canBeUsed;

    [SerializeField] protected SpriteRenderer spriteRenderer;
    [SerializeField] private Color onMouseOverColor, pressColor;
    [SerializeField] private Color defaultColor;
    private Vector3 originalScale, targetScale;

    private void Start()
    {
        originalScale = transform.localScale;
        targetScale = transform.localScale;
        if (spriteRenderer == null)
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        defaultColor = spriteRenderer.color;

        eventInstance += PressColorChange; 
        eventInstance += ChangeTargetScaleSmall;
    }

    //PRESS RELATED METHODS

    private void PressColorChange()
    {
        spriteRenderer.color = pressColor;
    }

    private void ChangeTargetScaleSmall()
    {
        targetScale = originalScale / 1.25f;
    }

    // CONTONIOUS METHODS

    private void Update()
    {
        if (transform.localScale != targetScale) transform.localScale = Vector3.Lerp(transform.localScale, targetScale, 0.3f * Time.deltaTime * 50);
        //if mouse is currently over this GameObject
        if (canBeUsed)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {               
                eventInstance();
            }
            else if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                //resetting to defaults
                targetScale = originalScale;
                spriteRenderer.color = onMouseOverColor;
            }
        }
    }

    // ON MOUSE OVER METHODS


    private void OnMouseEnter()
    {
        spriteRenderer.color = onMouseOverColor; 
        canBeUsed = true;
    }

    private void OnMouseExit()
    {
        //user may still hold LMB thus it has to be reset
        targetScale = originalScale;
        spriteRenderer.color = defaultColor;
        canBeUsed = false;
    }
}
