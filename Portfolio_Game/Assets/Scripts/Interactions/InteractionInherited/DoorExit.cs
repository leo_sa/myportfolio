using UnityEngine;

public class DoorExit : Interactable
{   
    void Start()
    {
        //InteractEventInstance is executed when interacted with this gameObject
        InteractEventInstance += ExitGame;  //Game will be closed on interaction through delegate subscription
    }

    private void ExitGame()
    {
        Debug.Log("ExitingGame");
        Application.Quit();
    }
}
