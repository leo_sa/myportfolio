using UnityEngine;

public class Page3D : Interactable
{
    //OTHER CLASSES
    [SerializeField] private Transform holdTransform;       //the transform to parent to when holding

    //VECTORS

    [SerializeField] private Vector3 holdPos, holdRot;      //rotation and position while holding
    [SerializeField] private Vector3 downPos, downRot;      //rotation and position while "down"
    private Vector3 currentPos, currentRot;
    private Vector3 translationVectorRot;      //an additional vector is needed due to the reason how unity handles rotation 

    //FLOATS

    [SerializeField, Range(0.01f, 0.3f)] private float lerpSpeedHolding;
    [SerializeField] private float sensitivityMultiplier;
    [SerializeField] private float rotationDegrees;     //Inertia in degrees
    private float mx, my;       //Mouse axis X and Y


    //BOOLEANS

    private bool isHeld = true;
    private bool activate;

    private void Start()
    {
        InteractEventInstance += ActivateHolding;
    }

    public void ActivateHolding()
    {
        translationVectorRot = transform.localEulerAngles;
        transform.parent = holdTransform;
        SwitchRotAndPos();
        activate = true;
    }

    private void Update()
    {
       
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            SwitchRotAndPos();
        }
    }

    private void FixedUpdate()
    {
        if (activate)
        {
            LerpToCurrentPosAndRot();   //put in FixedUpdate for clean inertia rotation
        }
    }

    //switches currenPos and currentRot based on isHeld
    private void SwitchRotAndPos()
    {
        //Defines which variables to use based on current input
        float? cachedSensitivMul = isHeld ? 0 : sensitivityMultiplier;
        Vector3? cachedPos = isHeld ? downPos : holdPos;
        Vector3? cachedRot = isHeld ? downRot : holdRot;

        //sets the variables accordingly
        currentPos = cachedPos.Value;
        currentRot = cachedRot.Value;
        ServiceLocator.managerUI.ChangeCursorStatus(!isHeld);
        ServiceLocator.fpsCamInstance.ChangeSensitivity(cachedSensitivMul.Value);
        ServiceLocator.playerInteractionInstance.canBeUsed = isHeld;
        isHeld = !isHeld;
    }

    //Lerps to current Rotation and position accordingly
    private void LerpToCurrentPosAndRot()
    {
        if (transform.localPosition != currentPos)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, currentPos, lerpSpeedHolding * Time.deltaTime * 50);
        }

        if (transform.localEulerAngles != CurrentRotation())
        {
            translationVectorRot = Vector3.Lerp(translationVectorRot, CurrentRotation(), lerpSpeedHolding * Time.deltaTime * 25);
            transform.localRotation = Quaternion.Euler(translationVectorRot);
        }
    }

    private Vector3 CurrentRotation()
    {
        return currentRot + InertiaVector();
    }

    private Vector3 InertiaVector()
    {
        //Getting inertia through mouseInput
        mx = Input.GetAxis("Mouse X") * 50 * Time.deltaTime;
        my = Input.GetAxis("Mouse Y") * 50 * Time.deltaTime;

        //Allpying inertia
        return new Vector3(rotationDegrees * -mx, 0, rotationDegrees * my);
    }

}
