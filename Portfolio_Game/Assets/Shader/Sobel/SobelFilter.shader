﻿Shader "CustomImageEffects/MySobel"
{
	//This Shader was inspired by, and made with the help of the tutorial:  https://www.youtube.com/watch?v=RMt6DcaMxcE
	//This Shader should be used as a post-processing effect.
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Thickness("Outline thickness", float) = 1
		_DepthStrength("Depth strength", float) = 1
		_DepthThickness("Depth thickness", float) = 1
		_DepthThreshHold("Depth threshhold", float) = 1
		_OutlineColor("Outline Color", color) = (0, 0, 0, 0)
    }
    SubShader
    {
        Tags {
				"RenderType" = "Opaque"
		}

        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"	//needed for ClipPos

			// SOBEL-ALGORYTHM RELATED

			static float2 sobelSamplePoints[9] = {
					float2(-1, 1), float2(0, 1), float2(1, 1),
					float2(-1, 0), float2(0, 0), float2(1, 1),
					float2(-1, 1), float2(0, -1), float2(1, -1),
			};

			static float sobelXMatrix[9] = {
				1, 0, -1,
				2, 0, -2,
				1, 0, -1

			};

			static float sobelYMatrix[9] = {
				1, 2, 1,
				0, 0, 0,
				-1, -2, -1

			};


			//STRUCTS

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };


			//ADDITIONAL METHODS

			//source: https://docs.unity3d.com/Packages/com.unity.shadergraph@6.9/manual/Blend-Node.html
			void BlendOverlay(float4 Base, float4 BlendFactor, float Opacity, out float4 Out) {

				float4 result1 = 1.0 - 2.0 * (1.0 - Base) * (1.0 - BlendFactor);
				float4 result2 = 2.0 * Base * BlendFactor;
				float4 zeroOrOne = step(Base, 0.5);
				Out = result2 * zeroOrOne + (1 - zeroOrOne) * result1;
				Out = lerp(Base, Out, Opacity);
			}


			float _DepthStrength;
			float _DepthThreshHold;
			float _DepthThickness;

			float PolishedSobel(float input) {

				input = smoothstep(0, _DepthThreshHold, input);
				input = pow(input, _DepthThickness);
				input = mul(_DepthStrength, input);

				return input;
			}


			sampler2D _CameraDepthTexture;	//Getting the depth directly from the Z-Buffer
			float _Thickness;

			void SobelExecution(float2 UV, out float Out)
			{
				float2 sobel = 0;

				//Creating the 9 'instances' 
				[unroll]
				for (int i = 0; i < 9; i++)
				{
					float depth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, UV + sobelSamplePoints[i] * _Thickness);
					sobel += depth * float2(sobelXMatrix[i], sobelYMatrix[i]);
				}

				Out = length(sobel);
			}

			//VERTEX AND FRAG FUNCTIONS

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float4 _OutlineColor;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }			

            float4 frag (v2f i) : SV_Target
            {
				float cachedValue;		//having a CachedValue for readability purposes
                float4 sceneRender = tex2D(_MainTex, i.uv);

				SobelExecution(i.uv, cachedValue);				
				//Interpolates final values, giving the result
				BlendOverlay(sceneRender, _OutlineColor, PolishedSobel(cachedValue), sceneRender);

                return sceneRender;
            }		    
            ENDHLSL
        }
    }
}
